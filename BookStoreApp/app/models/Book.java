package models;

import java.util.HashSet;
import java.util.Set;
import java.lang.*;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.*;

import com.avaje.ebean.Model;
@Entity
public class Book extends Model{
	@Id
	@Constraints.Required
	public Integer id;
	@Constraints.MaxLength(255)
	@Constraints.MinLength(5)
	@Constraints.Required
	public String title;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Constraints.Required
	@Constraints.Min(5)
	@Constraints.Max(100)
	public Integer price;
	@Constraints.Required
	public String author;
	public static Finder<Integer,Book> find =new Finder<>(Book.class);
	
}
