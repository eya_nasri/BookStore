package controllers;

import java.util.List;
import views.html.errors.*;
import java.util.Set;

import javax.inject.Inject;

import models.Book;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.books.*;

public class BooksController extends Controller {
	@Inject
	FormFactory formFactory;
	
//for all books	
public Result index()
{
List<Book> books=Book.find.all();

return ok(index.render(books));
}
//to create a book

public Result create()
{Form<Book> bookForm=formFactory.form(Book.class);
	
return ok(create.render(bookForm));}

//to save a book
public Result save()
{Form<Book> bookForm=formFactory.form(Book.class).bindFromRequest();
if(bookForm.hasErrors()) {
	flash("danger","Please Correct the Form Below ");
	return badRequest(create.render(bookForm));
}

Book book =bookForm.get();
book.save();
flash("success","Book Save Successfully");
return redirect(routes.BooksController.index());


}
//to edit a book
public Result edit(Integer id)
{
	Book book=Book.find.byId(id);
	if(book==null) {return notFound(views.html.errors._404.render());}
	Form<Book> bookForm =formFactory.form(Book.class).fill(book);
	
	return ok(edit.render(bookForm));
	
}
//to update
public Result update()
{
Form<Book> bookForm =formFactory.form(Book.class).bindFromRequest();
if (bookForm.hasErrors())
{
flash("danger","Please Correct the Form Below");
return badRequest(edit.render(bookForm));
}
Book book= bookForm.get();
Book oldBook=Book.find.byId(book.id);
if(oldBook==null) {
	
	flash("danger","Book Not Found");
	return notFound();}
oldBook.title=book.title;
oldBook.price=book.price;
oldBook.author=book.author;
oldBook.update();
flash("success","Book updated successfully");

return ok();
}

//to destroy
public Result destroy(Integer id)
{Book book=Book.find.byId(id);
if(book==null) {
	flash("danger","Book Not Found");
	return notFound();}
flash("success","Book Deleted");
book.delete();
	
return ok();}
//or book details
public Result show(Integer id)
{Book book=Book.find.byId(id);
if (book==null)
{return notFound(views.html.errors._404.render());}

return ok(show.render(book));}

}
